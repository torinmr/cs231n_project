python -m tensorflow.python.tools.optimize_for_inference ^
--input=built_graph\tiny-yolo-voc.pb ^
--output=built_graph\optimized_tiny-yolo-voc.pb ^
--input_names="input" ^
--output_names="output"