set tf_files=tensorflow-for-poets-2\tf_files

python -m tensorflow.python.tools.optimize_for_inference ^
--input=%tf_files%\retrained_graph.pb ^
--output=%tf_files%\optimized_graph.pb ^
--input_names=Placeholder ^
--output_names=final_result