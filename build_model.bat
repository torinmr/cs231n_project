set tf_files=tensorflow-for-poets-2\tf_files
set summary_dir=mobilenet

python retrain.py ^
--bottleneck_dir=%tf_files%\bottlenecks ^
--how_many_training_steps=4000 ^
--model_dir=%tf_files%\models ^
--summaries_dir=%tf_files%\training_summaries\%summary_dir% ^
--output_graph=%tf_files%\retrained_graph.pb ^
--output_labels=%tf_files%\retrained_labels.txt ^
--tfhub_module=https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/1 ^
--image_dir=..\data\krause_cars\labeled_ims_cropped
