import os

from imageio import imread
import matplotlib.pyplot as plt
import matplotlib.patches as patches

padding = 16

def load_image(a):
    return imread(os.path.join('..', 'data', 'krause_cars', 'ims', a.img_name))

def load_image_cropped(a):
    img = load_image(a)
    
    x_min = max(a.x_min - padding, 0)
    x_max = min(a.x_max + padding, img.shape[1])
    y_min = max(a.y_min - padding, 0)
    y_max = min(a.y_max + padding, img.shape[0])
    
    return img[y_min:y_max, x_min:x_max]

def disp_image(img):
    plt.imshow(img)
    plt.axis('off')
    plt.show()
    
def display_with_bounding_box(a):
    img = load_image(a)
    fig, ax = plt.subplots(1)
    ax.imshow(img)
    rect = patches.Rectangle((a.x_min, a.y_min),
                             a.x_max - a.x_min,
                             a.y_max - a.y_min,
                             linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rect)
    plt.axis('off')
    plt.show()