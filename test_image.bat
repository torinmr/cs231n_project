# Usage: test_image.bat model_name.pb

set tf_files=tensorflow-for-poets-2\tf_files

python label_image.py ^
--graph=%tf_files%\%1 ^
--labels=%tf_files%\retrained_labels.txt ^
--input_layer=Placeholder ^
--output_layer=final_result ^
--input_height=224 ^
--input_width=224 ^
--image="..\data\krause_cars\labeled_ims_cropped\Acura Integra Type R 2001\000405.jpg"