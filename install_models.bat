# Usage: install_models.bat model_name.pb

set tf_files=tensorflow-for-poets-2\tf_files
set assets=tensorflow-for-poets-2\android\tfmobile\assets

copy %tf_files%\%1 %assets%\graph.pb
copy %tf_files%\retrained_labels.txt %assets%\labels.txt